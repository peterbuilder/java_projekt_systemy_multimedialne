package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Huffman{

    public static void run(String input)
    {
        String[] array = input.split("");
        ArrayList<InputObject> inputObjectChar = new ArrayList<>();
        ArrayList<String> uniqueChars          = new ArrayList<>();
        ArrayList<InputObject> results         = new ArrayList<>();
        ArrayList<InputObject> resultFinal     = new ArrayList<>();

        //tworzymy tablicę z unikalnymi wartościami z podanego ciągu
        for (String arrayChar: array) {
            if (!uniqueChars.contains(arrayChar)) {
                uniqueChars.add(arrayChar);
                InputObject inputObject = new InputObject();
                inputObject.setInputChar(arrayChar);
                inputObjectChar.add(inputObject);
            }
        }

        //zliczamy wystąpienia poszczególnych znaków
        for (String arrayChar: array) {
            for (InputObject inputObject: inputObjectChar) {
                if (inputObject.getInputChar().equals(arrayChar)) {
                    inputObject.setOccurrence(inputObject.getOccurrence() + 1);
                }
            }
        }

        //obliczamy prawdopodobieństwo wystąpienia poszczególnych znaków
        int numberOfArrayChars = array.length;
        for (InputObject inputObject: inputObjectChar) {
            inputObject.setProbability(inputObject.getOccurrence() / (float)numberOfArrayChars);
        }

        Collections.sort(inputObjectChar);
        for (int i = 0; i < inputObjectChar.size(); i++) {
            InputObject sum = new InputObject();

            if (i == 0) {
                sum.setProbability(inputObjectChar.get(i).getProbability() + inputObjectChar.get(i + 1).getProbability());

                if (inputObjectChar.get(i).getProbability() >= inputObjectChar.get(i + 1).getProbability()) {
                    inputObjectChar.get(i).setResult("1");
                    inputObjectChar.get(i + 1).setResult("0");
                    results.add(inputObjectChar.get(i));
                    results.add(inputObjectChar.get(i + 1));
                    results.add(sum);
                    i++;
                } else {
                    inputObjectChar.get(i).setResult("0");
                    inputObjectChar.get(i + 1).setResult("1");
                    results.add(inputObjectChar.get(i));
                    results.add(inputObjectChar.get(i + 1));
                    i++;
                    results.add(sum);
                }
            } else if (i == 2) {
                if (results.get(i).getProbability() <= inputObjectChar.get(i).getProbability()) {
                    results.get(i).setResult("0");
                    inputObjectChar.get(i).setResult("1");
                    results.add(inputObjectChar.get(i));
                } else {
                    results.get(i).setResult("1");
                    inputObjectChar.get(i).setResult("0");
                    results.add(inputObjectChar.get(i));
                }

                sum.setProbability(results.get(i).getProbability() + inputObjectChar.get(i).getProbability());
                results.add(sum);
            } else {
                if (results.get(i+1).getProbability() <= inputObjectChar.get(i).getProbability()) {
                    results.get(i+1).setResult("0");
                    inputObjectChar.get(i).setResult("1");
                    results.add(inputObjectChar.get(i));
                } else {
                    results.get(i+1).setResult("1");
                    inputObjectChar.get(i).setResult("0");
                    results.add(inputObjectChar.get(i));
                }

                sum.setProbability(results.get(i+1).getProbability() + inputObjectChar.get(i).getProbability());
                results.add(sum);
                i++;
            }
        }

        // odwrócenie kolejności elementów w tablicy
        Collections.reverse(results);

        // zapisujemy kod dla każdego znaku
        int j = 0;
        String stringResult = "";
        for (InputObject result: results) {
            if (j == 1) {
                resultFinal.add(result);
            } else if ((j != 0) && (j % 2 == 1) || (j == results.size() - 1)) {
                result.setResult(stringResult + result.getResult());
                resultFinal.add(result);
            } else if ((j != 0) && (j % 2 == 0)) {
                stringResult += result.getResult();
            }

            j++;
        }

        for (InputObject result: resultFinal) {
            System.out.println("Znak: " + result.getInputChar() + "; Kod: " + result.getResult());
        }
    }

}

class InputObject implements Comparable<InputObject> {
    String inputChar; //znak który wprowadził użykownik
    Float probability; //prawdopodobieństwo wystąpienia w tablicy
    int occurrence = 0; //liczba wystąpień znaku
    String result; // kod końcowy znaku

    public String getInputChar() {
        return inputChar;
    }

    public void setInputChar(String inputChar) {
        this.inputChar = inputChar;
    }

    public float getProbability() {
        return probability;
    }

    public int getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(int occurrence) {
        this.occurrence = occurrence;
    }

    public void setProbability(Float probability) {
        this.probability = probability;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int compareTo(InputObject another) {
        // price fields should be Float instead of float
        return this.probability.compareTo(another.probability);
    }


}

class connection {
    InputObject inputObject;
    boolean connectionStatus; //stan połączenia z innym obiektem, 0 lub 1
} 

class objectConnection {
    InputObject inputObject;

}