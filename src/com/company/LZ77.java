package com.company;

import java.util.ArrayList;

public class LZ77 {
    public static void run(String input)
    {
        String[] array = input.split("");
        ArrayList<Step> result = new ArrayList<>();
        ArrayList<Integer> matchedCharsPosition = new ArrayList<>();
        ArrayList<Integer> matchedCharsPositionCopy = new ArrayList<>();

        for (int i = 0; i < array.length; i++) {
            Step step = new Step();


            if (i == 0) {
                step.setStep(i);
                step.setPosition(i);
                step.setStringChar(array[i]);
                step.setOutput("0,0," + array[i]);
                result.add(step);
                continue;
            } else {
                step.setStep(i);
                step.setPosition(Integer.parseInt(result.get(i-1).getOutput().split(",")[0]) + result.get(i-1).getPosition() + 1);

                // j - 1, ponieważ szukamy do pozycji wcześniej o 1 znak
                for (int j = 0; j <= step.getPosition() - 1; j++) {
                    if (array[j].equals(array[step.getPosition()])) {
                        matchedCharsPosition.add(j);

                        int k = 1;
                        for (j++; j <= step.getPosition() - 1; j++) {
                            if ((array[j].equals(array[step.getPosition() + k])) && ((step.getPosition() + k) < (array.length - 1))) {
                                matchedCharsPosition.add(j);
                                k++;
                            } else {
                                if (matchedCharsPositionCopy.size() < matchedCharsPosition.size()) {
                                    matchedCharsPositionCopy.clear();
                                    matchedCharsPositionCopy.addAll(matchedCharsPosition);
                                }
                                matchedCharsPosition.clear();
                                j--;
                                break;
                            }
                        }
                        if ((step.getPosition() + k) == (array.length - 1)) break;
                    }
                }

                if (matchedCharsPositionCopy.size() > matchedCharsPosition.size()) {
                    matchedCharsPosition.addAll(matchedCharsPositionCopy);
                    matchedCharsPositionCopy.clear();
                }

                if (!matchedCharsPosition.isEmpty()) {
                    for (int position: matchedCharsPosition) {
                        step.addMatch(array[position]);
                    }
                    step.setStringChar(array[step.getPosition() + matchedCharsPosition.size()]);
                    step.setOutput(matchedCharsPosition.size() + ",-" + (step.getPosition() - matchedCharsPosition.get(0)) + "," + array[step.getPosition() + matchedCharsPosition.size()]);
                } else {
                    step.setStringChar(array[step.getPosition()]);
                    step.setOutput("0,0," + step.getStringChar());
                }
            }

            result.add(step);

            //jeśli obsłużyliśmy ostatni znak, to wychodzimy z pętli for
            if (array.length <= step.getPosition() + matchedCharsPosition.size() + 1) break;

            matchedCharsPosition.clear();
        }

        for (Step item: result) {
            System.out.print("Krok: " + item.getStep() + "; Pozycja: " + item.getPosition() + "; Dopasowanie: ");

            for (String match: item.getMatch()) {
                System.out.print(match + " ");
            }

            System.out.println("; Znak: " + item.getStringChar() + "; Wyjście: " + item.getOutput());
        }
    }
}

class Step {
    private int step;
    private int position;
    private ArrayList<String> match = new ArrayList<>();
    private String stringChar;
    private String output;

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public ArrayList<String> getMatch() {
        return match;
    }

    public void setMatch(ArrayList<String> match) {
        this.match = match;
    }

    public void addMatch(String match) {
        this.match.add(match);
    }

    public String getStringChar() {
        return stringChar;
    }

    public void setStringChar(String stringChar) {
        this.stringChar = stringChar;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }
}


